package ru.spb.beavers.core.components;

import javax.swing.*;
import java.awt.*;

/**
 * Панель фиксированного размера, позволяющая задать его единожды.
 */
public class FixedJPanel extends JPanel {

    private static final Dimension defaultDimension = new JPanel().getSize();

    @Override
    public void setSize(Dimension dimension) {
        if (getSize().equals(defaultDimension)) {
            super.setSize(dimension);
        } else {
            throw new RuntimeException("Попытка изменения размеров фиксированной панели!");
        }
    }

    @Override
    public void setPreferredSize(Dimension preferredSize) {
        if (getSize().equals(defaultDimension)) {
            super.setPreferredSize(preferredSize);
        } else {
            throw new RuntimeException("Попытка изменения размеров фиксированной панели!");
        }
    }

    @Override
    public void setSize(int width, int height) {
        if (getSize().equals(new Dimension(0,0))) {
            super.setSize(width, height);
        } else {
            throw new RuntimeException("Попытка изменения размеров фиксированной панели!");
        }
    }

    @Override
    public void setMinimumSize(Dimension minimumSize) {
        if (getMinimumSize().equals(defaultDimension)) {
            super.setSize(minimumSize);
        } else {
            throw new RuntimeException("Попытка изменения размеров фиксированной панели!");
        }
    }

    @Override
    public void setMaximumSize(Dimension maximumSize) {
        if (getMaximumSize().equals(defaultDimension)) {
            super.setSize(maximumSize);
        } else {
            throw new RuntimeException("Попытка изменения размеров фиксированной панели!");
        }
    }
}
